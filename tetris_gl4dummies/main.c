#include <stdlib.h>
#include <stdio.h>
#include <ncurses.h>	

// NUMBER OF BLOC IN A TETROMINOS
#define TETRIS 4
// NUMBER OF TETROMINOS IN TETROMINOS
#define NUMBER_TETROMINOS 7
// NUMBER OF ORIENTATION FOR EACH PIECE OF TETROMINOS
#define NUMBER_OF_ORIENTATION 4

int * pointer;

typedef struct Bloc Bloc;
struct Bloc 
{
    int **tableaux;
    int width,row,col;
};
Bloc current;

typedef struct Dimension Dimension;
struct Dimension 
{
    int w;
    int h;
};
Dimension Dim;

const Bloc BlocTableaux[24] = {
    
    {(int *[]){(int []){0,1,1},(int []){1,1,0}, (int []){0,0,0}}, 3},                                //S_shape     
    {(int *[]){(int []){1,1,0},(int []){0,1,1}, (int []){0,0,0}}, 3},                               //Z_shape     
    {(int *[]){(int []){0,1,0},(int []){0,1,1}, (int []){0,1,0}}, 3},                              //T_shape     
    {(int *[]){(int []){0,1,0},(int []){0,1,0}, (int []){0,1,1}}, 3},                             //L_shape        
    {(int *[]){(int []){1,1},(int []){1,1}}, 2},                                                //SQ_shape
    {(int *[]){(int []){0,0,0,0}, (int []){1,1,1,1}, (int []){0,0,0,0}, (int []){0,0,0,0}}, 4}, //I_shape

    // first rotation

    {(int *[]){(int []){0,0,1},(int []){0,1,1}, (int []){0,1,0}}, 3},                                //S_shape     
    {(int *[]){(int []){0,0,1},(int []){0,1,1}, (int []){0,1,0}}, 3},                               //Z_shape     
    {(int *[]){(int []){0,1,0},(int []){1,1,1}, (int []){0,0,0}}, 3},                              //T_shape     
    {(int *[]){(int []){0,0,1},(int []){1,1,1}, (int []){0,0,0}}, 3},                             //L_shape        
    {(int *[]){(int []){1,1},(int []){1,1}}, 2},                                                //SQ_shape
    {(int *[]){(int []){0,1,0,0}, (int []){0,1,0,0}, (int []){0,1,0,0}, (int []){0,1,0,0}}, 4}, //I_shape

    //second rotation

    {(int *[]){(int []){0,1,1},(int []){1,1,0}, (int []){0,0,0}}, 3},                                //S_shape     
    {(int *[]){(int []){1,1,0},(int []){0,1,1}, (int []){0,0,0}}, 3},                               //Z_shape     
    {(int *[]){(int []){1,1,1},(int []){0,1,0}, (int []){0,0,0}}, 3},                              //T_shape     
    {(int *[]){(int []){0,0,0},(int []){1,1,1}, (int []){1,0,0}}, 3},                             //L_shape        
    {(int *[]){(int []){1,1},(int []){1,1}}, 2},                                                //SQ_shape
    {(int *[]){(int []){0,0,0,0}, (int []){1,1,1,1}, (int []){0,0,0,0}, (int []){0,0,0,0}}, 4}, //I_shape

    //third rotation

    {(int *[]){(int []){0,1,1},(int []){1,1,0}, (int []){0,0,0}}, 3},                                //S_shape     
    {(int *[]){(int []){1,1,0},(int []){0,1,1}, (int []){0,0,0}}, 3},                               //Z_shape     
    {(int *[]){(int []){0,1,0},(int []){1,1,0}, (int []){0,1,0}}, 3},                              //T_shape     
    {(int *[]){(int []){0,1,0},(int []){0,1,0}, (int []){1,1,0}}, 3},                             //L_shape        
    {(int *[]){(int []){1,1},(int []){1,1}}, 2},                                                //SQ_shape
    {(int *[]){(int []){0,0,0,0}, (int []){1,1,1,1}, (int []){0,0,0,0}, (int []){0,0,0,0}}, 4},

};
void send_to_tab(int b[])
{
    int i,j;
    for(i = 0; i < current.width ;i++){
        for(j = 0; j < current.width ; j++){
            if(current.tableaux[i][j])
                b[(current.row+i) * Dim.w + (current.col+j)] = current.tableaux[i][j];
        }
    }
};


Bloc CopyBloc(Bloc bloc)
{
    Bloc new_bloc = bloc;
    int **copybloc = bloc.tableaux;
    new_bloc.tableaux = (int**)malloc(new_bloc.width*sizeof(int*));
    int i, j;
    for(i = 0; i < new_bloc.width; i++){
        new_bloc.tableaux[i] = (int*)malloc(new_bloc.width*sizeof(int));
        for(j=0; j < new_bloc.width; j++) {
            new_bloc.tableaux[i][j] = copybloc[i][j];
        }
    }
    return new_bloc;
};

void DeleteBloc(Bloc bloc)
{
    int i;
    for(i = 0; i < bloc.width; i++){
        free(bloc.tableaux[i]);
    }
    free(bloc.tableaux);
};

void Move(char c)
{
    switch(c){
        case 's':
            current.row++;  //move down
            to_tab(pointer);
            break;
        case 'd':
            current.col++;  //move right
            break;
        case 'a':
            current.col--;  //move left
            break;
        case 'w':
            break;
    }
    //DeleteBloc(current);
};

void GetNewBloc()
{
    int rng = rand() % 7;
    Bloc new_bloc = CopyBloc(BlocTableaux[rng]);

    new_bloc.col = 1;
    new_bloc.row = 0;
    DeleteBloc(current);
    current = new_bloc;
};

unsigned int * board(int w, int h)
{
    int i,j;
    Dim.w = w;
    Dim.h = h; 
    int * board = w * h - 1;
    board = malloc(w * h * sizeof *board);
    GetNewBloc();
    for (i = 0; i < h; ++i) 
    {
        for (j = 0; j < w; ++j)
        {
            board[i * Dim.w + j] = 0;
        }
    }
    pointer = board;
    //to_tab(board);



    return (unsigned int*)board;
};
